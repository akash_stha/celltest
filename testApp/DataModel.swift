//
//  info.swift
//  testApp
//
//  Created by Newarpunk on 1/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

class DataModel {
    enum StudentState  : Int {
        case none
        case present
        case absent
        case late
        case leave
    }
    
    var name: String
    var state: StudentState
    
    init(state: StudentState, name: String) {
        
        self.state = state
        self.name = name
    }
}

// case present = 0, absent, late, leave, none

