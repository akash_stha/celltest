//
//  TableViewCell.swift
//  testApp
//
//  Created by Newarpunk on 1/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

protocol StudentStateDelegate {
    func buttonClicked(state: DataModel.StudentState, indexPath : IndexPath?)
}

class TableViewCell : UITableViewCell {
    
    var delegateValue : StudentStateDelegate?
    var indexPath : IndexPath?
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var onPresent: UIButton!
    @IBOutlet weak var onAbsent: UIButton!
    @IBOutlet weak var onLeave: UIButton!
    @IBOutlet weak var onLate: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func presentClicked(_ sender: UIButton) {
//        onPresent.backgroundColor = .darkGray
//        onPresent.setTitleColor(UIColor.white, for: .normal)
//        onAbsent.backgroundColor = .groupTableViewBackground
//        onAbsent.setTitleColor(UIColor.black, for: .normal)
//        onLeave.backgroundColor = .groupTableViewBackground
//        onLeave.setTitleColor(UIColor.black, for: .normal)
//        onLate.backgroundColor = .groupTableViewBackground
//        onLate.setTitleColor(UIColor.black, for: .normal)
        setStudentForClass(state: .present)
        delegateValue?.buttonClicked(state: .present, indexPath: indexPath)
    }
    
    @IBAction func absentClicked(_ sender: UIButton) {
//        onPresent.backgroundColor = .groupTableViewBackground
//        onPresent.setTitleColor(UIColor.black, for: .normal)
//        onAbsent.backgroundColor = .darkGray
//        onAbsent.setTitleColor(UIColor.white, for: .normal)
//        onLeave.backgroundColor = .groupTableViewBackground
//        onLeave.setTitleColor(UIColor.black, for: .normal)
//        onLate.backgroundColor = .groupTableViewBackground
//        onLate.setTitleColor(UIColor.black, for: .normal)
        setStudentForClass(state: .absent)
        delegateValue?.buttonClicked(state: .absent, indexPath: indexPath)
    }
    
    @IBAction func lateClicked(_ sender: UIButton) {
//        onPresent.backgroundColor = .groupTableViewBackground
//        onPresent.setTitleColor(UIColor.black, for: .normal)
//        onAbsent.backgroundColor = .groupTableViewBackground
//        onAbsent.setTitleColor(UIColor.black, for: .normal)
//        onLeave.backgroundColor = .groupTableViewBackground
//        onLeave.setTitleColor(UIColor.black, for: .normal)
//        onLate.backgroundColor = .darkGray
//        onLate.setTitleColor(UIColor.white, for: .normal)
        setStudentForClass(state: .late)
        delegateValue?.buttonClicked(state: .late, indexPath: indexPath)
    }
    
    @IBAction func leaveClicked(_ sender: UIButton) {
//        onPresent.backgroundColor = .groupTableViewBackground
//        onPresent.setTitleColor(UIColor.black, for: .normal)
//        onAbsent.backgroundColor = .groupTableViewBackground
//        onAbsent.setTitleColor(UIColor.black, for: .normal)
//        onLeave.backgroundColor = .darkGray
//        onLeave.setTitleColor(UIColor.white, for: .normal)
//        onLate.backgroundColor = .groupTableViewBackground
//        onLate.setTitleColor(UIColor.black, for: .normal)
        setStudentForClass(state: .leave)
        delegateValue?.buttonClicked(state: .leave, indexPath: indexPath)
    }
    
    func configureCell(dataModel: DataModel) {
        nameLabel.text = dataModel.name
        setStudentForClass(state: dataModel.state)
    }
    
    func setStudentForClass(state: DataModel.StudentState) {
        
        switch state {
            case .present:
                onPresent.backgroundColor = .darkGray
                onPresent.setTitleColor(UIColor.white, for: .normal)
                onAbsent.backgroundColor = .groupTableViewBackground
                onAbsent.setTitleColor(UIColor.black, for: .normal)
                onLeave.backgroundColor = .groupTableViewBackground
                onLeave.setTitleColor(UIColor.black, for: .normal)
                onLate.backgroundColor = .groupTableViewBackground
                onLate.setTitleColor(UIColor.black, for: .normal)

            case .absent:
                onPresent.backgroundColor = .groupTableViewBackground
                onPresent.setTitleColor(UIColor.black, for: .normal)
                onAbsent.backgroundColor = .darkGray
                onAbsent.setTitleColor(UIColor.white, for: .normal)
                onLeave.backgroundColor = .groupTableViewBackground
                onLeave.setTitleColor(UIColor.black, for: .normal)
                onLate.backgroundColor = .groupTableViewBackground
                onLate.setTitleColor(UIColor.black, for: .normal)
            case .late:
                onPresent.backgroundColor = .groupTableViewBackground
                onPresent.setTitleColor(UIColor.black, for: .normal)
                onAbsent.backgroundColor = .groupTableViewBackground
                onAbsent.setTitleColor(UIColor.black, for: .normal)
                onLeave.backgroundColor = .groupTableViewBackground
                onLeave.setTitleColor(UIColor.black, for: .normal)
                onLate.backgroundColor = .darkGray
                onLate.setTitleColor(UIColor.white, for: .normal)
            case .leave:
                onPresent.backgroundColor = .groupTableViewBackground
                onPresent.setTitleColor(UIColor.black, for: .normal)
                onAbsent.backgroundColor = .groupTableViewBackground
                onAbsent.setTitleColor(UIColor.black, for: .normal)
                onLeave.backgroundColor = .darkGray
                onLeave.setTitleColor(UIColor.white, for: .normal)
                onLate.backgroundColor = .groupTableViewBackground
                onLate.setTitleColor(UIColor.black, for: .normal)
            case .none:
                onPresent.backgroundColor = .groupTableViewBackground
                onPresent.setTitleColor(UIColor.black, for: .normal)
                onAbsent.backgroundColor = .groupTableViewBackground
                onAbsent.setTitleColor(UIColor.black, for: .normal)
                onLeave.backgroundColor = .groupTableViewBackground
                onLeave.setTitleColor(UIColor.black, for: .normal)
                onLate.backgroundColor = .groupTableViewBackground
                onLate.setTitleColor(UIColor.black, for: .normal)
        }
    }
    
}
