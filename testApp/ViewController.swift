//
//  ViewController.swift
//  testApp
//
//  Created by Newarpunk on 1/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, StudentStateDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let allDataSource = DataHere()
    
    var present : Int = 0
    var absent : Int = 0
    var late : Int = 0
    var leave : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
//        tableView.allowsMultipleSelection = true
        
    }
    
    @IBAction func totalCountClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Count", message: "Total Present: \(present) \n Total Absent: \(absent) \n Total late: \(late) \n On Leave: \(leave)", preferredStyle: .alert)
        let addAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(addAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let totalCount = allDataSource.dataSource.count
        return totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        let model = allDataSource.dataSource[indexPath.item]
        cell.configureCell(dataModel: model)
        cell.setStudentForClass(state: model.state)
        
        cell.selectionStyle = .blue
        
        cell.delegateValue = self
        cell.indexPath = indexPath
        return cell
    }
    
    func buttonClicked(state: DataModel.StudentState, indexPath: IndexPath?) {
        if let indexPath = indexPath {
            let model = allDataSource.dataSource[indexPath.item]
            model.state = state
            print("state is \(state)")
        }
 
    }
}
